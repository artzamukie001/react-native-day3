import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch,Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import ex6 from './ex6'
import profile from './profile'


class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/ex6" component={ex6} />
                    <Route exact path="/profile" component={profile} />
                    <Route exact path="/screen1" component={Screen1} />
                    <Route exact path="/screen2" component={Screen2} />
                    <Redirect to="/ex6" />
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router