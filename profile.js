import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet } from 'react-native'
import { Link } from 'react-router-native'

class profile extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <View style={styles.header1}>
                        <Link to='/ex6'>
                            <Text style={styles.text}>+++</Text>
                        </Link>
                    </View>
                    <View style={styles.header2}>
                        <Text style={styles.text}>My Profile</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <Text style={styles.text}>User name</Text>
                    <Text style={styles.text}>{this.props.location.state.member}</Text>
                    <Text style={styles.text}>First name</Text>
                    <Text style={styles.text}>Last name</Text>
                </View>

                <View style={styles.footer}>
                    <View style={styles.edit}>
                        <Text style={styles.text}>Edit</Text>
                    </View>
                </View>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'yellow',
        flex: 1
    },
    header1: {
        backgroundColor: 'gray',
        height: 40,
        flex: 1,
        margin: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    header2: {
        backgroundColor: 'gray',
        flex: 10,
        height: 40,
        margin: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    text: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
    content: {
        backgroundColor: 'black',
        flex: 5
    },
    footer: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    edit: {
        backgroundColor: 'gray',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 60,
        margin: 14,
    }
})

export default profile